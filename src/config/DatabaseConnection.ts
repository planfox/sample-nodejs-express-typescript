'use strict';
import pg = require('pg')

export interface IDatabaseConnection {
    connect(): void;
    disconnect(): void;
}

const className = "DatabaseConnection";
export class DatabaseConnection implements IDatabaseConnection {

    constructor(){

    }

    public connect(): void {
        console.log(`${className}:connect`);
    }

    public disconnect(): void {
        console.log(`${className}:disconnect`);
    }
}